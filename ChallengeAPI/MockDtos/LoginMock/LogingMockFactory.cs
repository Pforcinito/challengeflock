﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.MockDtos.LoginMock
{
    public class LogingMockFactory
    {
        public LoginMock MockLogin { get; set; }

        public LoginMock CreateMockLogin()
        {
            return new LoginMock();
        }
    }
}
