﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.MockDtos.LoginMock
{
    public class LoginMock : User
    {
        public LoginMock()
        {
            UserName = "PabloForci";
            Password = "HolaMundo01";
        }
    }
}
