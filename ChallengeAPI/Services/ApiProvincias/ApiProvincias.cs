﻿using ChallengeAPI.Contracts.ApiConsume;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Services.ApiProvincias
{
    public class ApiProvincias : ApiProvinciasGeneric
    {

        public async Task<T> GetByName<T>(string name)
        {
            var client = new RestClient(Url);
            var request = new RestRequest(Param.Replace("{value}", name), Method.GET);
            var response = await client.ExecuteAsync(request);

            return JsonConvert.DeserializeObject<T>(response.Content);
        }

    }
}
