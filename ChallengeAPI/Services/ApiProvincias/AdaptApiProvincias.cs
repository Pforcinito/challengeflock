﻿using ChallengeAPI.Contracts.ApiConsume;
using ChallengeAPI.Dtos.ApiProvDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Services.ApiProvincias
{
    public class AdaptApiProvincias : IApiGetByName<Task<ProvinciaResponse>>
    {

        private readonly ApiProvincias _apiProvincias;


        public AdaptApiProvincias(ApiProvincias apiProvincias)
        {
            _apiProvincias = apiProvincias;
        }

        public Task<ProvinciaResponse> GetByName(string param)
        {
            return _apiProvincias.GetByName<ProvinciaResponse>(param);
        }

    }
}
