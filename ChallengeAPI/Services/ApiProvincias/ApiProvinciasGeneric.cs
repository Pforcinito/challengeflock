﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Services.ApiProvincias
{
    public class ApiProvinciasGeneric
    {
        public string  Url => "https://apis.datos.gob.ar/georef/api/provincias";

        public string Param => "?nombre={value}";
    }
}
