﻿using ChallengeAPI.Contracts;
using ChallengeAPI.Contracts.ApiConsume;
using ChallengeAPI.Dtos.ApiProvDtos;
using ChallengeAPI.Services.ApiProvincias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Services
{
    public class ProvinciasApiServices : IProvinciaService
    {
        private readonly ILoggerManager _logger;
        private readonly IApiGetByName<Task<ProvinciaResponse>> _apiProv;

        public ProvinciasApiServices(ILoggerManager logger)
        {
            _logger = logger;
            _apiProv = new AdaptApiProvincias(new ApiProvincias.ApiProvincias());
        }

        public async Task<ProvinciaResponse> GetByName(string provName)
        {
            _logger.LogInfo("ApiProvincias request");

            _logger.LogDebug($"Provincia Name: {provName}");

            ProvinciaResponse response = null;
            try
            {
                _logger.LogDebug($"Searching provincia by name {provName}");

                response = await _apiProv.GetByName(provName);

            }
            catch(Exception ex)
            {
                _logger.LogError($"Error at get provincia by name. Message: {ex.Message}", ex);
            }

            return response;
            
        }
    }
}
