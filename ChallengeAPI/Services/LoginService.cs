﻿using ChallengeAPI.Contracts;
using ChallengeAPI.Dtos.LoginDtos;
using ChallengeAPI.MockDtos.LoginMock;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ChallengeAPI.Services
{
    public class LoginService : ILoginService
    {
        private readonly ILoggerManager _logger;
        private readonly IConfiguration _configuration;         
        private readonly LogingMockFactory _mockLogin;

        public LoginService(IConfiguration configuration, ILoggerManager logger)
        {
            _configuration = configuration;
            _logger = logger;
            _mockLogin = new LogingMockFactory();
        }

        public LoginResponse Login(LoginRequest req)
        {
            _logger.LogInfo("Login request");
            _logger.LogDebug($"User try login: {req.Username}");

            var res = CheckValidUser(req);

            var login =  new LoginResponse()
            {
                ResultSuccess = res,
                ResultMessage = res ? "Login success" : "Login unsucces",
                ResultCode = res ? 1 : 0,
                Token = res ? GenerateToken(req.Username) : string.Empty
            };

            _logger.LogInfo($"Login result succes:{login.ResultSuccess} ");
            _logger.LogInfo($"Login result code:{login.ResultCode} ");
            _logger.LogInfo($"Login result message:{login.ResultMessage} ");

            return login;
        }

        private bool CheckValidUser(LoginRequest req)
        {
            var User = _mockLogin.CreateMockLogin();

            return User.UserName.ToLower() == req.Username.ToLower() && User.Password == req.Password;
        }

        private string GenerateToken(string username)
        {
            _logger.LogDebug("Create token");

            var secretKey = _configuration.GetValue<string>("SecretKey");
            var duration = _configuration.GetValue<int>("AuthenticationTime");
            var key = Encoding.ASCII.GetBytes(secretKey);

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, username)
            };

            var claimsIdentity = new ClaimsIdentity(claims);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claimsIdentity,
                Expires = DateTime.UtcNow.AddMinutes(duration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            _logger.LogDebug("Token created");

            return tokenHandler.WriteToken(createdToken);
        }
    }
}
