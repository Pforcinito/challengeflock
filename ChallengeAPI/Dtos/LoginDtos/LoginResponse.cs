﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Dtos.LoginDtos
{
    public class LoginResponse : Response
    {
        public string Token { get; set; }
    }
}
