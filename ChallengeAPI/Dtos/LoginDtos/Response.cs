﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Dtos.LoginDtos
{
    public abstract class Response
    {
        public bool ResultSuccess { get; set; }
        public int ResultCode { get; set; }
        public string ResultMessage { get; set; }
    }
}
