﻿using ChallengeAPI.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [Authorize]
    public class ProvinciasApiController : ControllerBase
    {

        private readonly ILoggerManager _logger;
        private readonly IProvinciaService _service;

        public ProvinciasApiController(IProvinciaService service, ILoggerManager logger)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet("{name}")]
        public async Task<IActionResult> GetProvinciaByName(string name)
        {
            _logger.LogInfo("GetProvincia Request");
            _logger.LogDebug($"Name: {name}");

            var result = await _service.GetByName(name);

            //_logger.LogInfo($"Result: {result.Success}");
            //_logger.LogInfo($"Code: {result.ResultCode}");

            //if (result == null || result.ResultCode != 0)
                //return NotFound(result);

            return Ok(result);
        }
    }
}
