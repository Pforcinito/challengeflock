﻿using ChallengeAPI.Contracts;
using ChallengeAPI.Dtos.LoginDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [AllowAnonymous]
    public class LoginController : ControllerBase
    {
        private readonly ILoggerManager _logger;
        private readonly ILoginService _service;

        public LoginController(ILoggerManager logger, ILoginService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpPost]
        public  IActionResult Login(LoginRequest request)
        {
            _logger.LogInfo("Login request");

            var result = _service.Login(request);

            if (result.ResultSuccess != true)
                return BadRequest(result);

            return Ok(result);
        }
    }
}
