﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Contracts
{
    public interface ILoggerManager
    {
        //Metodo Genera Mensaje Tipo Informativo 
        //con parametro un mensaje
        void LogInfo(string message);

        //Metodo Genera Mensaje Tipo Informativo 
        //con parametro un mensaje y objeto Excepcion
        void LogInfo(string message, Exception ex);

        //Metodo Genera Mensaje Debug 
        //con parametro un mensaje
        void LogDebug(string message);

        //Metodo Genera Mensaje Debug 
        //con parametro un mensaje
        void LogDebug(string message, Exception ex);

        //Metodo Genera Mensaje Tipo Advertencia 
        //con parametro un mensaje
        void LogWarn(string message);

        //Metodo Genera Mensaje Tipo Advertencia 
        //con parametro un mensaje y objeto Excepcion
        void LogWarn(string message, Exception ex);

        //Metodo Genera Mensaje Tipo Error 
        //con parametro un mensaje
        void LogError(string message);

        //Metodo Genera Mensaje Tipo Error 
        //con parametro un mensaje y objeto Excepcion
        void LogError(string message, Exception ex);

    }
}
