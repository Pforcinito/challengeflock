﻿using ChallengeAPI.Dtos.LoginDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Contracts
{
    public interface ILoginService
    {
        public LoginResponse Login(LoginRequest req); 
    }
}
