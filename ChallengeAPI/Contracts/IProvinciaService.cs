﻿using ChallengeAPI.Dtos.ApiProvDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Contracts
{
    public interface IProvinciaService
    {
        public Task<ProvinciaResponse> GetByName(string provName);
    }
}
