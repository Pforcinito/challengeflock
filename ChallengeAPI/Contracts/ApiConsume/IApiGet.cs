﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Contracts.ApiConsume
{
    public interface IApiGetByName<T>
    {
        public T GetByName(string param);
    }

    //public interface IApiPost<T, U>
    //{
    //    public T Post(U body);
    //}

    //public interface IApiPut<T, U>
    //{
    //    public T Put(U body);
    //}

}
