﻿using ChallengeAPI.Contracts;
using ChallengeAPI.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeAPI.Extentions
{
    public static class IoC
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IProvinciaService, ProvinciasApiServices>();
        }
    }
}
