** Challenge Api Provincias **

Esta api contiene dos endpoints para loguearse con autenticacion y otro para consultar las coordenadas de una provincia por nombre de esta.


Login:
- Metodo "login" se envia por POST con un body con usuario y contraseña.

Ejemplo:
	URL: https://localhost:44356/api/login
	Request: 
	{
		"username":"prueba",
		"password":"sad1234+"
	}

En caso de exito devuelve un toke para utilizar la autenticacion del endpoint ProvinciaAPI.


ProvinciaAPI:
- Metodo "GetByName" devuelve las coordenadas por medio de un GET. Tiene un unico parametro, el nombre de provincia a buscar.

Ejemplo:
	URL: https://localhost:50000/api/provincia/salta

En caso de exito devuelve las coordenadas


Para mas informacion acerca de los request y posibles responses, verificar la documentacion en https://[URL]/swagger/index.html


Comentarios:
Utilice Swagger para la doc de la API, JWT para la autenticacion, NUnit para los test y RestSharp para consumir la api del gobierno.
Por otro lado, aplique un Adapter para la reutilizacion de codigo, inyeccion de dependencia, segregacion de interfaz y metodos de extension (agregado como para demostrar otros conocimientos).
En un contexto con base de datos, hubiera usado EFCore con LINQ y AutoMapper, como minimo.

Otros comentarios:
En otro contexto hubiera estimado mas tiempo de entrega, y le hubiera aplicado mas cosas. Estoy seguro que lo podria haber hecho mucho mejor, pero por cuestiones de tiempo debo entregarlo asi (creo que esta bastante completo).




