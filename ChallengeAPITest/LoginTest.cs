using ChallengeAPI.Contracts;
using ChallengeAPI.Controllers;
using ChallengeAPI.Dtos.LoginDtos;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ChallengeAPITest
{
    public class LoginTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Login_Success()
        {
            var moqValidUser = new LoginRequest()
            {
                Username = "pabloForci",
                Password = "HolaMundo1235+"
            };

            var mockLogger = new Mock<ILoggerManager>();
            var mockLogin = new Mock<ILoginService>();

            mockLogin.Setup(loginRepo => loginRepo.Login(moqValidUser)).Returns(LoginSuccess());

            var controller = new LoginController(mockLogger.Object, mockLogin.Object);

            var result = controller.Login(moqValidUser) as OkObjectResult;

            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.IsTrue(result.StatusCode == 200);

            var response = result.Value as LoginResponse;

            Assert.IsTrue(response.ResultSuccess);
            Assert.IsNotEmpty(response.Token);
        }

        [Test]
        public void Login_Unsuccess()
        {
            var moqInvalidUser = new LoginRequest()
            {
                Username = "admin2",
                Password = "admin"
            };

            var mockLogger = new Mock<ILoggerManager>();
            var mockLogin = new Mock<ILoginService>();

            mockLogin.Setup(loginRepo => loginRepo.Login(moqInvalidUser)).Returns(LoginUnsuccess());

            var controller = new LoginController(mockLogger.Object, mockLogin.Object);

            var result = controller.Login(moqInvalidUser) as OkObjectResult;

            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.IsTrue(result.StatusCode == 200);

            var resultDto = result.Value as LoginResponse;

            Assert.IsFalse(resultDto.ResultSuccess);
            Assert.IsEmpty(resultDto.Token);
        }

        private LoginResponse LoginSuccess() => new LoginResponse()
        {
            ResultSuccess = true,
            ResultCode = 1,
            ResultMessage = "",
            Token = "asdfagasd-gdsdsgsdgsd"
        };

        private LoginResponse LoginUnsuccess() => new LoginResponse()
        {
            ResultSuccess = false,
            ResultCode = 0,
            ResultMessage = "Invalid login",
            Token = string.Empty
        };
    }
}